package com.ranull.sittable.util;

import org.bukkit.block.BlockFace;

public final class BlockFaceUtil {
    public static int getBlockFaceYaw(BlockFace blockFace) {
        switch (blockFace) {
            case SOUTH:
                return 0;
            case SOUTH_WEST:
                return 45;
            case WEST:
                return 90;
            case NORTH_WEST:
                return 135;
            case EAST:
                return -90;
            case NORTH_EAST:
                return -135;
            case SOUTH_EAST:
                return -45;
            default:
                return 180;
        }
    }
}
