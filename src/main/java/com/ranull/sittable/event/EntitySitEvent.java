package com.ranull.sittable.event;

import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class EntitySitEvent extends Event implements Cancellable {
    private static final HandlerList HANDLERS = new HandlerList();
    private final Entity entity;
    private final Entity seatEntity;
    private final Type type;
    private boolean cancel;

    public EntitySitEvent(Entity entity, Entity seatEntity, Type type) {
        this.entity = entity;
        this.seatEntity = seatEntity;
        this.type = type;
    }

    @NotNull
    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @NotNull
    public Entity getEntity() {
        return entity;
    }

    @NotNull
    public Entity getSeatEntity() {
        return seatEntity;
    }

    @NotNull
    public Type getType() {
        return type;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    @Override
    public boolean isCancelled() {
        return cancel;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }

    public enum Type {
        GROUND,
        BLOCK
    }
}
