package com.ranull.sittable.event;

import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class EntityStandEvent extends Event {
    private static final HandlerList HANDLERS = new HandlerList();
    private final Entity entity;
    private final Entity seatEntity;

    public EntityStandEvent(Entity entity, Entity seatEntity) {
        this.entity = entity;
        this.seatEntity = seatEntity;
    }

    @NotNull
    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @NotNull
    public Entity getEntity() {
        return entity;
    }

    @NotNull
    public Entity getSeatEntity() {
        return seatEntity;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }
}
