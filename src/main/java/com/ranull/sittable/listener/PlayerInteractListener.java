package com.ranull.sittable.listener;

import com.ranull.sittable.Sittable;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class PlayerInteractListener implements Listener {
    private final Sittable plugin;

    public PlayerInteractListener(Sittable plugin) {
        this.plugin = plugin;
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        ItemStack itemInHand = player.getInventory().getItemInHand();
        Block block = event.getClickedBlock();

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK
                && (!plugin.getVersionManager().hasSecondHand() || event.getHand() == EquipmentSlot.HAND)
                && player.getGameMode() != GameMode.SPECTATOR
                && !player.isSneaking() && !plugin.getSitManager().isEntitySitting(player)
                && (itemInHand.getType() == Material.AIR || !itemInHand.getType().isBlock())
                && block != null && !block.getRelative(BlockFace.UP).getType().isSolid()
                && plugin.getSitManager().isLocationSittable(block.getLocation())
                && !plugin.getSitManager().isBlockOccupied(block)
                && player.hasPermission("sittable.sit.block")
                && (plugin.getWorldGuard() == null || plugin.getWorldGuard().isSittable(block.getLocation()))) {
            if (plugin.getSitManager().isWithinSittableDistance(player, block)) {
                plugin.getSitManager().sitOnBlock(player, block);
                event.setCancelled(true);
            } else {
                plugin.sendMessage(player, plugin.getConfig().getString("messages.sit-failure-distance"));
            }
        }
    }
}
