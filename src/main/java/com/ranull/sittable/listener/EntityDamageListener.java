package com.ranull.sittable.listener;

import com.ranull.sittable.Sittable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class EntityDamageListener implements Listener {
    private final Sittable plugin;

    public EntityDamageListener(Sittable plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onEntityDamage(EntityDamageEvent event) {
        if (plugin.getSitManager().isVehicleSittable(event.getEntity())) {
            event.setCancelled(true);
        }
    }
}
