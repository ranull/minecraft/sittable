package com.ranull.sittable.listener;

import com.ranull.sittable.Sittable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EntityDamageByEntityListener implements Listener {
    private final Sittable plugin;

    public EntityDamageByEntityListener(Sittable plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (plugin.getSitManager().isEntitySitting(event.getEntity())) {
            plugin.getSitManager().stopSitting(event.getEntity());
        }
    }
}
