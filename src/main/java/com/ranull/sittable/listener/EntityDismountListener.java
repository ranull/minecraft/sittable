package com.ranull.sittable.listener;

import com.ranull.sittable.Sittable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.spigotmc.event.entity.EntityDismountEvent;

public class EntityDismountListener implements Listener {
    private final Sittable plugin;

    public EntityDismountListener(Sittable plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityDismount(EntityDismountEvent event) {
        if (plugin.getSitManager().isVehicleSittable(event.getDismounted())) {
            plugin.getSitManager().stopSitting(event.getEntity(), event.getDismounted());
        }
    }
}
