package com.ranull.sittable.listener;

import com.ranull.sittable.Sittable;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {
    private final Sittable plugin;

    public BlockBreakListener(Sittable plugin) {
        this.plugin = plugin;
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        for (Entity entity : event.getBlock().getWorld().getNearbyEntities(event.getBlock().getLocation()
                .add(0.5, 0.5, 0.5), 0.49, 0.49, 0.49)) {
            if (plugin.getSitManager().isVehicleSittable(entity) && entity.getPassenger() != null) {
                plugin.getSitManager().stopSitting(entity.getPassenger());
            }
        }
    }
}
