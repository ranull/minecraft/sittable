package com.ranull.sittable.compatibility;

import org.bukkit.Location;
import org.bukkit.block.Block;

public interface Compatibility {
    void correctBlockLocation(Location location, Block block, double offset);
}
