package com.ranull.sittable.compatibility;

import com.ranull.sittable.util.BlockFaceUtil;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Stairs;

@SuppressWarnings("deprecation")
public class CompatibilityMaterialData implements Compatibility {
    @Override
    public void correctBlockLocation(Location location, Block block, double offset) {
        MaterialData materialData = block.getState().getData();

        if ((block.getType().toString().matches("(.*)_SLAB(.*)") || block.getType().toString().matches("(.*)STEP"))
                && block.getData() < 8) {
            location.subtract(0.0, 0.5, 0.0);
        } else if (materialData instanceof Stairs && block.getData() < 4) {
            location.subtract(0.0, 0.5, 0.0);
            setStairsLocation(((Stairs) materialData), location, offset);
        } else if (block.getType().toString().matches("(.*)_BED") || block.getType().toString().equals("BED_BLOCK")) {
            location.subtract(0.0, 0.4, 0.0);
        } else if (block.getType().toString().matches("SKULL")) {
            location.subtract(0.0, 0.5, 0.0);
        } else if (block.getType().toString().matches("(.*)_PLATE")) {
            location.subtract(0.0, 1.0, 0.0);
        } else if (!block.getType().isSolid()) {
            location.subtract(0.0, 1.0, 0.0);
        }
    }

    private void setStairsLocation(Stairs stairs, Location location, double offset) {
        location.setYaw(BlockFaceUtil.getBlockFaceYaw(stairs.getFacing()));

        switch (stairs.getFacing()) {
            case NORTH:
                location.setYaw(BlockFaceUtil.getBlockFaceYaw(BlockFace.NORTH));
                location.subtract(0.0, 0.0, offset);

                break;
            case EAST:
                location.setYaw(BlockFaceUtil.getBlockFaceYaw(BlockFace.EAST));
                location.add(offset, 0.0, 0.0);

                break;
            case SOUTH:
                location.setYaw(BlockFaceUtil.getBlockFaceYaw(BlockFace.SOUTH));
                location.add(0.0, 0.0, offset);

                break;
            case WEST:
                location.setYaw(BlockFaceUtil.getBlockFaceYaw(BlockFace.WEST));
                location.subtract(offset, 0.0, 0.0);

                break;
        }
    }
}
