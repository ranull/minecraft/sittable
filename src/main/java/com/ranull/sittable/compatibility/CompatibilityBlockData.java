package com.ranull.sittable.compatibility;

import com.ranull.sittable.util.BlockFaceUtil;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Slab;
import org.bukkit.block.data.type.Stairs;

public class CompatibilityBlockData implements Compatibility {
    @Override
    public void correctBlockLocation(Location location, Block block, double offset) {
        BlockData blockData = block.getBlockData();

        if (blockData instanceof Slab && ((Slab) blockData).getType() == Slab.Type.BOTTOM) {
            location.subtract(0.0, 0.5, 0.0);
        } else if (blockData instanceof Stairs && ((Stairs) blockData).getHalf() == Bisected.Half.BOTTOM) {
            location.subtract(0.0, 0.5, 0.0);
            setStairsLocation(((Stairs) blockData), location, offset);
        } else if (block.getType().toString().matches("(.*)_BED") || block.getType().toString().equals("BED")) {
            location.subtract(0.0, 0.4, 0.0);
        } else if (block.getType().toString().matches("(.*)_HEAD") || block.getType().toString().matches("(.*)_SKULL")) {
            location.subtract(0.0, 0.5, 0.0);
        } else if (block.getType().toString().matches("(.*)_PRESSURE_PLATE")) {
            location.subtract(0.0, 1.0, 0.0);
        } else if (!block.getType().isSolid()) {
            location.subtract(0.0, 1.0, 0.0);
        }
    }

    private void setStairsLocation(Stairs stairs, Location location, double offset) {
        location.setYaw(BlockFaceUtil.getBlockFaceYaw(stairs.getFacing().getOppositeFace()));

        switch (stairs.getFacing()) {
            case NORTH:
                location.add(0.0, 0.0, offset);

                break;
            case EAST:
                location.subtract(offset, 0.0, 0.0);

                break;
            case SOUTH:
                location.subtract(0.0, 0.0, offset);

                break;
            case WEST:
                location.add(offset, 0.0, 0.0);

                break;
        }

        if (stairs.getShape() == Stairs.Shape.INNER_LEFT || stairs.getShape() == Stairs.Shape.OUTER_LEFT) {
            switch (stairs.getFacing()) {
                case NORTH:
                    location.setYaw(BlockFaceUtil.getBlockFaceYaw(BlockFace.SOUTH_EAST));
                    location.add(offset, 0.0, 0.0);

                    break;
                case EAST:
                    location.setYaw(BlockFaceUtil.getBlockFaceYaw(BlockFace.SOUTH_WEST));
                    location.add(0.0, 0.0, offset);

                    break;
                case SOUTH:
                    location.setYaw(BlockFaceUtil.getBlockFaceYaw(BlockFace.NORTH_WEST));
                    location.subtract(offset, 0.0, 0.0);

                    break;
                case WEST:
                    location.setYaw(BlockFaceUtil.getBlockFaceYaw(BlockFace.NORTH_EAST));
                    location.subtract(0.0, 0.0, offset);

                    break;
            }
        } else if (stairs.getShape() == Stairs.Shape.INNER_RIGHT || stairs.getShape() == Stairs.Shape.OUTER_RIGHT) {
            switch (stairs.getFacing()) {
                case NORTH:
                    location.setYaw(BlockFaceUtil.getBlockFaceYaw(BlockFace.SOUTH_WEST));
                    location.subtract(offset, 0.0, 0.0);

                    break;
                case EAST:
                    location.setYaw(BlockFaceUtil.getBlockFaceYaw(BlockFace.NORTH_WEST));
                    location.subtract(0.0, 0.0, offset);

                    break;
                case SOUTH:
                    location.setYaw(BlockFaceUtil.getBlockFaceYaw(BlockFace.NORTH_EAST));
                    location.add(offset, 0.0, 0.0);

                    break;
                case WEST:
                    location.setYaw(BlockFaceUtil.getBlockFaceYaw(BlockFace.SOUTH_EAST));
                    location.add(0.0, 0.0, offset);

                    break;
            }
        }
    }
}
