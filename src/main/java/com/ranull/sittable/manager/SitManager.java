package com.ranull.sittable.manager;

import com.ranull.sittable.Sittable;
import com.ranull.sittable.event.EntitySitEvent;
import com.ranull.sittable.event.EntityStandEvent;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.PatternSyntaxException;

public class SitManager {
    private final Sittable plugin;

    public SitManager(Sittable plugin) {
        this.plugin = plugin;
    }

    public int cleanupArmorStands() {
        int count = 0;

        for (World world : plugin.getServer().getWorlds()) {
            for (Entity entity : world.getEntities()) {
                if (isVehicleSittable(entity)) {
                    entity.remove();
                    count++;
                }
            }
        }

        return count;
    }

    public void resetAllPlayers() {
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            if (player.getVehicle() != null && isVehicleSittable(player.getVehicle())) {
                standTeleportSafely(player, player.getVehicle());
                removeEntity(player.getVehicle());
                plugin.sendMessage(player, plugin.getConfig().getString("messages.stand-success"));
            }
        }
    }

    public boolean sitOnGround(Entity entity) {
        Location location = entity.getLocation();
        ArmorStand armorStand = spawnArmorStand(location.clone().add(0.0, -0.20
                + plugin.getConfig().getDouble("sit.offset.below"), 0.0));

        setLastLocation(armorStand, location);

        EntitySitEvent entitySitEvent = new EntitySitEvent(entity, armorStand, EntitySitEvent.Type.GROUND);

        plugin.getServer().getPluginManager().callEvent(entitySitEvent);

        if (!entitySitEvent.isCancelled()) {
            addPassenger(armorStand, entity);

            if (entity instanceof Player && plugin.getVersionManager().hasSwingHand()
                    && plugin.getConfig().getBoolean("sit.animation.ground")) {
                ((Player) entity).swingMainHand();
            }

            plugin.sendMessage(entity, plugin.getConfig().getString("messages.sit-success"));

            if (entity instanceof LivingEntity) {
                runRegenerationTask((LivingEntity) entity, plugin.getConfig().getInt("sit.regeneration.ground"));
            }

            return true;
        } else {
            removeEntity(armorStand);
        }

        return false;
    }

    public boolean sitOnBlock(Entity entity, Block block) {
        return sitOnBlock(entity, block, 0.0, 0.0, 0.0);
    }

    public boolean sitOnBlock(Entity entity, Block block, double offsetX, double offsetY, double offsetZ) {
        return sitOnBlock(entity, block, offsetX, offsetY, offsetZ,
                entity.getLocation().getYaw());
    }

    public boolean sitOnBlock(Entity entity, Block block, double offsetX, double offsetY, double offsetZ, float yaw) {
        Location location = block.getLocation().clone().add(0.5 + offsetX, 0.80 + offsetY + plugin.getConfig()
                .getDouble("sit.offset.below"), 0.5 + offsetZ);

        location.setYaw(yaw);
        plugin.getCompatibility().correctBlockLocation(location, block, 0.12 + plugin.getConfig()
                .getDouble("sit.offset.behind"));

        ArmorStand armorStand = spawnArmorStand(location);
        setLastLocation(armorStand, entity.getLocation());

        EntitySitEvent entitySitEvent = new EntitySitEvent(entity, armorStand, EntitySitEvent.Type.BLOCK);

        plugin.getServer().getPluginManager().callEvent(entitySitEvent);

        if (!entitySitEvent.isCancelled()) {
            if (plugin.getConfig().getBoolean("sit.camera.block")) {
                Location entityLocation = entity.getLocation().clone();

                entityLocation.setYaw(armorStand.getLocation().getYaw());
                entity.teleport(entityLocation);
            }

            addPassenger(armorStand, entity);

            if (entity instanceof Player && plugin.getVersionManager().hasSwingHand()
                    && plugin.getConfig().getBoolean("sit.animation.block")) {
                ((Player) entity).swingMainHand();
            }

            plugin.sendMessage(entity, plugin.getConfig().getString("messages.sit-success"));

            if (entity instanceof LivingEntity) {
                runRegenerationTask((LivingEntity) entity, plugin.getConfig().getInt("sit.regeneration.block"));
            }

            return true;
        } else {
            removeEntity(armorStand);
        }

        return false;
    }

    public void stopSitting(Entity entity) {
        if (entity.getVehicle() != null && isVehicleSittable(entity.getVehicle())) {
            stopSitting(entity, entity.getVehicle());
        }
    }

    public void stopSitting(Entity entity, Entity vehicleEntity) {
        EntityStandEvent entityStandEvent = new EntityStandEvent(entity, vehicleEntity);

        plugin.getServer().getPluginManager().callEvent(entityStandEvent);
        plugin.getServer().getScheduler().runTask(plugin, () -> {
            standTeleportSafely(entity, vehicleEntity);
            removeEntity(vehicleEntity);
            plugin.sendMessage(entity, plugin.getConfig().getString("messages.stand-success"));
        });
    }

    private boolean isLocationSafe(Location location) {
        Block block = location.getBlock();
        Block aboveBlock = location.getBlock().getRelative(BlockFace.UP);
        Block belowBlock = location.getBlock().getRelative(BlockFace.DOWN);

        return !block.getType().isSolid() && !aboveBlock.getType().isSolid() && belowBlock.getType().isSolid();
    }

    private void removeEntity(Entity entity) {
        if (entity != null) {
            entity.remove();
        }
    }

    public ArmorStand spawnArmorStand(Location location) {
        if (location.getWorld() != null) {
            if (plugin.getVersionManager().hasSpawnConsumer()) {
                return location.getWorld().spawn(location, ArmorStand.class, (armorStand) -> {
                    armorStand.setVisible(false);
                    armorStand.setGravity(false);
                    armorStand.setInvulnerable(true);
                    armorStand.setMarker(true);
                    armorStand.setCustomName("sittable|" + location.getWorld().getName() + "|"
                            + location.getX() + "|" + location.getY() + "|" + location.getZ());
                    Objects.requireNonNull(armorStand.getAttribute(Attribute.GENERIC_MAX_HEALTH)).setBaseValue(1);

                    if (plugin.getVersionManager().hasScoreboardTags()) {
                        armorStand.getScoreboardTags().add("sittableArmorStand");
                    }
                });
            } else {
                ArmorStand armorStand = location.getWorld().spawn(location, ArmorStand.class);

                armorStand.setVisible(false);
                armorStand.setGravity(false);
                armorStand.setMarker(true);
                armorStand.setCustomName("sittable|" + location.getWorld().getName() + "|"
                        + location.getX() + "|" + location.getY() + "|" + location.getZ());

                if (plugin.getVersionManager().hasAttributeInstance()) {
                    Objects.requireNonNull(armorStand.getAttribute(Attribute.GENERIC_MAX_HEALTH)).setBaseValue(1);
                }

                return armorStand;
            }
        }

        return null;
    }

    @SuppressWarnings("deprecation")
    public boolean isBlockOccupied(Block block) {
        for (Entity entity : block.getWorld().getNearbyEntities(block.getLocation()
                .add(0.5, 0.5, 0.5), 0.49, 0.49, 0.49)) {
            try {
                if (isVehicleSittable(entity) && entity.getPassengers().size() > 0) {
                    return true;
                }
            } catch (NoSuchMethodError ignored) {
                if (isVehicleSittable(entity) && entity.getPassenger() != null) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isVehicleSittable(Entity entity) {
        return entity instanceof ArmorStand && entity.getName().startsWith("sittable|");
    }

    public boolean isEntitySitting(Entity entity) {
        return entity.getVehicle() != null && isVehicleSittable(entity.getVehicle());
    }

    public boolean isEntityOnGround(Entity entity) {
        return entity.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().isSolid();
    }

    public boolean isWithinSittableDistance(Entity entity, Block block) {
        return entity.getLocation().distance(block.getLocation()) <= plugin.getConfig().getDouble("sit.distance");
    }

    public boolean isLocationSittable(Location location) {
        try {
            return plugin.getConfig().getStringList("sit.materials").stream()
                    .anyMatch(string -> location.clone().getBlock().getType().toString().matches(string));
        } catch (PatternSyntaxException exception) {
            plugin.getLogger().warning("Misconfiguration: " + exception.getMessage());
        }

        return false;
    }

    private Location getLastLocation(Entity entity) {
        if (entity.getName().startsWith("sittable|") && entity instanceof ArmorStand) {
            String locationString = entity.getName().replace("sittable|", "");
            String[] cords = locationString.split("\\|");

            try {
                World world = plugin.getServer().getWorld(UUID.fromString(cords[0]));
                double x = Double.parseDouble(cords[1]);
                double y = Double.parseDouble(cords[2]);
                double z = Double.parseDouble(cords[3]);

                return new Location(world, x, y, z);
            } catch (IllegalArgumentException ignored) {
            }
        }

        return null;
    }

    private void setLastLocation(Entity entity, Location location) {
        if (location.getWorld() != null) {
            entity.setCustomName("sittable|" + location.getWorld().getUID() + "|"
                    + location.getX() + "|" + location.getY() + "|" + location.getZ());
        }
    }

    private void standTeleportSafely(Entity entity, Entity vehicleEntity) {
        Location vehicleLocation = vehicleEntity.getLocation().clone();

        vehicleLocation.setY(Math.floor(vehicleEntity.getLocation().getY()));

        if (plugin.getConfig().getBoolean("stand.return")) {
            Location location = getLastLocation(vehicleEntity);

            if (location != null && isLocationSafe(location)) {
                teleportLocation(entity, location);

                return;
            }
        }


        if (shouldStandNearby(vehicleLocation)) {
            Location location = getStandLocationFront(vehicleLocation);

            if (location != null) {
                teleportLocation(entity, location);

                return;
            }

            location = getStandLocationNearby(vehicleLocation);

            if (location != null) {
                teleportLocation(entity, location);

                return;
            }

            location = getStandLocationNearby(vehicleLocation.clone().subtract(0.0, 1, 0.0));

            if (location != null) {
                teleportLocation(entity, location);

                return;
            }
        }

        Location location = getStandLocationAbove(vehicleLocation);

        if (location != null) {
            teleportLocation(entity, location);
        }
    }

    private boolean shouldStandNearby(Location location) {
        return location.clone().add(0.0, 0.5, 0.0).getBlock().getType().isSolid();
    }

    private Location getStandLocationFront(Location location) {
        location = location.clone().add(location.getDirection());

        return isLocationSafe(location) ? location : null;
    }

    private Location getStandLocationNearby(Location location) {
        Block block = location.getBlock();
        List<Location> locationList = new ArrayList<>();

        locationList.add(block.getRelative(BlockFace.NORTH).getLocation().add(0.5, 0.0, 0.5));
        locationList.add(block.getRelative(BlockFace.EAST).getLocation().add(0.5, 0.0, 0.5));
        locationList.add(block.getRelative(BlockFace.SOUTH).getLocation().add(0.5, 0.0, 0.5));
        locationList.add(block.getRelative(BlockFace.WEST).getLocation().add(0.5, 0.0, 0.5));

        return locationList.stream().filter(listLocation -> isLocationSafe(listLocation)
                && !listLocation.clone().add(0, 2, 0).getBlock().getType().isSolid()).findFirst().orElse(null);
    }

    private Location getStandLocationAbove(Location location) {
        location = location.clone();

        location.setY(location.getBlockY() + 1);

        return isLocationSafe(location) ? location : null;
    }

    private void teleportLocation(Entity entity, Location location) {
        if (location != null && location.getWorld() != null) {
            location.setYaw(entity.getLocation().getYaw());
            location.setPitch(entity.getLocation().getPitch());
            entity.teleport(location);
        }
    }

    @SuppressWarnings("deprecation")
    private void runRegenerationTask(LivingEntity livingEntity, int amplifier) {
        if (plugin.isEnabled() && amplifier > -1) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (isEntitySitting(livingEntity)) {
                        if (livingEntity.getHealth() < livingEntity.getMaxHealth()) {
                            livingEntity.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION,
                                    60, amplifier, true, false));
                        }
                    } else {
                        cancel();
                    }
                }
            }.runTaskTimer(plugin, 0L, 30L);
        }
    }

    @SuppressWarnings("deprecation")
    private void addPassenger(Entity entity, Entity passengerEntity) {
        try {
            entity.addPassenger(passengerEntity);
        } catch (NoSuchMethodError ignored) {
            entity.setPassenger(passengerEntity);
        }
    }
}
