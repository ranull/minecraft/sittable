package com.ranull.sittable.manager;

import org.bukkit.Bukkit;

public final class VersionManager {
    private final String version;
    private final boolean hasBlockData;
    private final boolean hasSwingHand;
    private final boolean hasSecondHand;
    private final boolean hasScoreboardTags;
    private final boolean hasSpawnConsumer;
    private final boolean hasAttributeInstance;

    public VersionManager() {
        this.version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        this.hasBlockData = !is_v1_7() && !is_v1_8() && !is_v1_9() && !is_v1_10() && !is_v1_11() && !is_v1_12();
        this.hasSwingHand = !is_v1_7() && !is_v1_8() && !is_v1_9() && !is_v1_10() && !is_v1_11() && !is_v1_12()
                && !is_v1_13() && !is_v1_14() && !is_v1_15();
        this.hasSecondHand = !is_v1_7() && !is_v1_8();
        this.hasScoreboardTags = !is_v1_7() && !is_v1_8() && !is_v1_9() && !is_v1_10();
        this.hasSpawnConsumer = !is_v1_7() && !is_v1_8() && !is_v1_9() && !is_v1_10();
        this.hasAttributeInstance = !is_v1_7() && is_v1_8();
    }

    public boolean hasBlockData() {
        return hasBlockData;
    }

    public boolean hasSwingHand() {
        return hasSwingHand;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean hasSecondHand() {
        return hasSecondHand;
    }

    public boolean hasScoreboardTags() {
        return hasScoreboardTags;
    }

    public boolean hasSpawnConsumer() {
        return hasSpawnConsumer;
    }

    public boolean hasAttributeInstance() {
        return hasAttributeInstance;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean is_v1_7() {
        return version.matches("(?i)v1_7_R1|v1_7_R2|v1_7_R3|v1_7_R4");
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean is_v1_8() {
        return version.matches("(?i)v1_8_R1|v1_8_R2|v1_8_R3");
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean is_v1_9() {
        return version.matches("(?i)v1_9_R1|v1_9_R2");
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean is_v1_10() {
        return version.matches("(?i)v1_10_R1");
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean is_v1_11() {
        return version.matches("(?i)v1_11_R1");
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean is_v1_12() {
        return version.matches("(?i)v1_12_R1");
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean is_v1_13() {
        return version.matches("(?i)v1_13_R1|v1_13_R2");
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean is_v1_14() {
        return version.matches("(?i)v1_14_R1");
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean is_v1_15() {
        return version.matches("(?i)v1_15_R1");

    }
}
