package com.ranull.sittable.command;

import com.ranull.sittable.Sittable;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class SitCommand implements CommandExecutor {
    private final Sittable plugin;

    public SitCommand(Sittable plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command,
                             @NotNull String string, @NotNull String[] args) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;

            if (player.hasPermission("sittable.sit.command")) {
                if (!plugin.getSitManager().isEntitySitting(player)) {
                    if (plugin.getSitManager().isEntityOnGround(player)) {
                        if (plugin.getWorldGuard() == null || plugin.getWorldGuard().isSittable(player.getLocation())) {
                            plugin.getSitManager().sitOnGround(player);
                        } else {
                            plugin.sendMessage(player, plugin.getConfig().getString("messages.sit-failure-worldguard"));
                        }
                    } else {
                        plugin.sendMessage(player, plugin.getConfig().getString("messages.sit-failure-unable"));
                    }
                } else {
                    plugin.getSitManager().stopSitting(player);
                }
            } else {
                plugin.sendMessage(commandSender, plugin.getConfig().getString("messages.plugin-permission"));
            }
        }

        return true;
    }
}
