package com.ranull.sittable.command;

import com.ranull.sittable.Sittable;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SittableCommand implements CommandExecutor, TabExecutor {
    private final Sittable plugin;

    public SittableCommand(Sittable plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command,
                             @NotNull String string, String[] args) {
        if (args.length == 0) {
            commandSender.sendMessage(ChatColor.GREEN + "Sittable " + ChatColor.DARK_GRAY
                    + ChatColor.RESET + ChatColor.DARK_GRAY + "v" + plugin.getDescription().getVersion());

            commandSender.sendMessage(ChatColor.GREEN + "/sittable " + ChatColor.DARK_GRAY + "-"
                    + ChatColor.RESET + " Plugin info");

            if (commandSender.hasPermission("sittable.reload")) {
                commandSender.sendMessage(ChatColor.GREEN + "/sittable reload " + ChatColor.DARK_GRAY + "-"
                        + ChatColor.RESET + " Reload plugin");
            }

            commandSender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GREEN + "Ranull");
        } else if (args[0].equals("reload")) {
            if (commandSender.hasPermission("sittable.reload")) {
                plugin.saveDefaultConfig();
                plugin.reloadConfig();
                plugin.sendMessage(commandSender, plugin.getConfig().getString("messages.plugin-reload"));
            } else {
                plugin.sendMessage(commandSender, plugin.getConfig().getString("messages.plugin-permission"));
            }
        } else if (args[0].equals("cleanup")) {
            if (commandSender.hasPermission("sittable.cleanup")) {
                int count = plugin.getSitManager().cleanupArmorStands();

                commandSender.sendMessage(ChatColor.GREEN + "Sittable" + ChatColor.DARK_GRAY + " » "
                        + ChatColor.RESET + "Cleaned up " + ChatColor.GREEN + count
                        + ChatColor.RESET + " seats.");
            } else {
                plugin.sendMessage(commandSender, plugin.getConfig().getString("messages.plugin-permission"));
            }
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, @NotNull Command command,
                                      @NotNull String string, @NotNull String[] args) {
        if (commandSender.hasPermission("sittable.reload")) {
            return Collections.singletonList("reload");
        }

        return new ArrayList<>();
    }
}
