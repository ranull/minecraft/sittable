package com.ranull.sittable;

import com.ranull.sittable.command.SitCommand;
import com.ranull.sittable.command.SittableCommand;
import com.ranull.sittable.compatibility.Compatibility;
import com.ranull.sittable.compatibility.CompatibilityBlockData;
import com.ranull.sittable.compatibility.CompatibilityMaterialData;
import com.ranull.sittable.integration.WorldGuard;
import com.ranull.sittable.listener.*;
import com.ranull.sittable.manager.SitManager;
import com.ranull.sittable.manager.VersionManager;
import com.ranull.sittable.util.BlockFaceUtil;
import net.md_5.bungee.api.ChatColor;
import org.bstats.bukkit.Metrics;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Sittable extends JavaPlugin {
    private static Sittable instance;
    private WorldGuard worldGuard;
    private VersionManager versionManager;
    private Compatibility compatibility;
    private SitManager sitManager;

    public static boolean sitOnGround(Entity entity) {
        return instance.getSitManager().sitOnGround(entity);
    }

    public static boolean sitOnBlock(Entity entity, Block block) {
        return instance.getSitManager().sitOnBlock(entity, block);
    }

    public static boolean sitOnBlock(Entity entity, Block block, double offsetX, double offsetY, double offsetZ) {
        return instance.getSitManager().sitOnBlock(entity, block, offsetX, offsetY, offsetZ);
    }

    public static boolean sitOnBlock(Entity entity, Block block, double offsetX, double offsetY, double offsetZ,
                                     BlockFace blockFace) {
        return instance.getSitManager().sitOnBlock(entity, block, offsetX, offsetY, offsetZ, BlockFaceUtil
                .getBlockFaceYaw(blockFace));
    }

    public static boolean sitOnBlock(Entity entity, Block block, double offsetX, double offsetY, double offsetZ,
                                     float yaw) {
        return instance.getSitManager().sitOnBlock(entity, block, offsetX, offsetY, offsetZ, yaw);
    }


    public static boolean stand(Entity entity) {
        return entity.leaveVehicle();
    }

    public static boolean isBlockOccupied(Block block) {
        return instance.getSitManager().isBlockOccupied(block);
    }

    public static boolean isEntitySitting(Entity entity) {
        return instance.getSitManager().isEntitySitting(entity);
    }

    @Override
    public void onLoad() {
        if (getServer().getPluginManager().getPlugin("WorldGuard") != null) {
            worldGuard = new WorldGuard(this);
        }
    }

    @Override
    public void onEnable() {
        instance = this;
        versionManager = new VersionManager();
        compatibility = versionManager.hasBlockData() ? new CompatibilityBlockData() : new CompatibilityMaterialData();
        sitManager = new SitManager(this);

        saveDefaultConfig();
        registerMetrics();
        registerCommands();
        registerListeners();
    }

    @Override
    public void onDisable() {
        sitManager.resetAllPlayers();
        unregisterListeners();
    }

    private void registerMetrics() {
        new Metrics(this, 15270);
    }

    public void registerListeners() {
        getServer().getPluginManager().registerEvents(new EntityDismountListener(this), this);
        getServer().getPluginManager().registerEvents(new EntityDamageListener(this), this);
        getServer().getPluginManager().registerEvents(new EntityDamageByEntityListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerInteractListener(this), this);
        getServer().getPluginManager().registerEvents(new BlockBreakListener(this), this);
    }

    public void unregisterListeners() {
        HandlerList.unregisterAll(this);
    }

    private void registerCommands() {
        PluginCommand sittablePluginCommand = getCommand("sittable");
        PluginCommand sitPluginCommand = getCommand("sit");

        if (sittablePluginCommand != null) {
            SittableCommand sittableCommand = new SittableCommand(this);

            sittablePluginCommand.setExecutor(sittableCommand);
            sittablePluginCommand.setTabCompleter(sittableCommand);
        }

        if (sitPluginCommand != null) {
            SitCommand sitCommand = new SitCommand(this);

            sitPluginCommand.setExecutor(sitCommand);
        }
    }

    public WorldGuard getWorldGuard() {
        return worldGuard;
    }

    public VersionManager getVersionManager() {
        return versionManager;
    }

    public Compatibility getCompatibility() {
        return compatibility;
    }

    public SitManager getSitManager() {
        return sitManager;
    }

    public void sendMessage(CommandSender commandSender, String string) {
        if (commandSender instanceof Player && string != null && !string.equals("")) {
            Player player = (Player) commandSender;
            String prefix = getConfig().getString("messages.prefix");

            if (prefix != null && !prefix.equals("")) {
                string = prefix + string;
            }

            Pattern pattern = Pattern.compile("&#[a-fA-f0-9]{6}");
            Matcher matcher = pattern.matcher(string);

            while (matcher.find()) {
                String colorHex = string.substring(matcher.start() + 1, matcher.end());
                string = string.replace("&" + colorHex, ChatColor.of(colorHex).toString());
                matcher = pattern.matcher(string);
            }

            player.sendMessage(string.replace("&", "§"));
        }
    }
}
